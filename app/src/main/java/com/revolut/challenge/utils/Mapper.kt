package com.revolut.challenge.utils

import com.revolut.challenge.R
import com.revolut.challenge.model.CurrencyModel
import com.revolut.challenge.service.models.Currency


/**
 * Transform Currency Object to with HashMap to ArrayList<CurrencyModel>
 */
fun Currency.transformToCurrencyList(
    base: String,
    amount: Double
): ArrayList<CurrencyModel> {
    val currencies = arrayListOf<CurrencyModel>()

    this.let {
        for (entry in this.rates.entries) {
            val key = entry.key
            val tab = entry.value
            currencies.add(
                CurrencyModel(
                    key,
                    Utils.getResId(key.toLowerCase(), R.drawable::class.java),
                    tab,
                    Utils.getStringName(key, R.string::class.java)
                )
            )
        }

        firstPosition(base, amount, currencies)
    }

    return currencies
}

/**
 * Add to first position
 */
private fun firstPosition(base: String, amount: Double, currencies: ArrayList<CurrencyModel>) {
    currencies.add(
        0,
        CurrencyModel(
            base,
            Utils.getResId(base.toLowerCase(), R.drawable::class.java),
            amount,
            Utils.getStringName(base, R.string::class.java)
        )
    )
}
