package com.revolut.challenge.utils

import android.util.Log
import com.revolut.challenge.R
import java.lang.reflect.Field

/**
 * Created by pcamilo on 01/05/2020.
 */
object Utils {

    /**
     * Get the resource image by filename and class R.drawable.(filename)
     */
    fun getResId(resName: String, c: Class<*>): Int {
        return try {
            val idField: Field? = if (resName == "try" || resName == "TRY") {
                c.getDeclaredField("try_currency") // Work around in this line because I can no use the word Java 'try'
            } else {
                c.getDeclaredField(resName)
            }

            idField?.getInt(idField)!!

        } catch (e: Exception) {
            Log.v(e.toString(), e.message.toString())
            R.drawable.ic_currency
        }
    }

    /**
     * Get String value with String identifier
     */
    fun getStringName(stringName: String, c: Class<*>): Int {
         return try {
            val idField: Field? = c.getDeclaredField(stringName)
            idField?.getInt(idField)!!
        } catch (e: Exception) {
            Log.v(e.toString(), e.message.toString())
            R.string.EUR
        }
    }
}