package com.revolut.challenge.service

import com.revolut.challenge.repository.base.ObservableResponse
import com.revolut.challenge.service.models.Currency
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by pcamilo on 01/05/2020.
 */
interface CurrencyService {

    /**
     * Get the latest currencies
     */
    @GET("latest")
    fun getCurrencies(@Query("base") base: String,
                          @Query("amount") amount: Double) : ObservableResponse<Currency>
}