package com.revolut.challenge.service

import com.revolut.challenge.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by pcamilo on 01/05/2020.
 */
class APIClient {

    private var mRetrofit: Retrofit

    init {
        mRetrofit = Retrofit
            .Builder()
            .baseUrl(BuildConfig.APP_BASE_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private fun getLoggingCapableHttpClient(): HttpLoggingInterceptor {
        val mLogging = HttpLoggingInterceptor()
        mLogging.level = HttpLoggingInterceptor.Level.BODY

        return mLogging
    }

    private fun getOkHttpClient() : OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(getLoggingCapableHttpClient())
            .build()
    }

    fun getRetrofit() = mRetrofit
}