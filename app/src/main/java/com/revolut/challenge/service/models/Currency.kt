package com.revolut.challenge.service.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by pcamilo on 01/05/2020.
 */
class Currency (@SerializedName("baseCurrency") var base: String,
                @SerializedName("rates") var rates: Rates) : Serializable {

    class Rates : HashMap<String, Double>()
}