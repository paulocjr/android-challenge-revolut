package com.revolut.challenge.service.models

/**
 * Created by pcamilo on 01/05/2020.
 */
class BaseResponse<T>(val success: Boolean, val data: T?)