package com.revolut.challenge.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.revolut.challenge.BR

/**
 * Created by pcamilo on 01/05/2020.
 */
data class CurrencyModel(private var _currency: String, private var _picture: Int ,
                         private var _amount: Double, private var _currencyName: Int) : BaseObservable() {

    var currency: String
        @Bindable get() = _currency
        set(value) {
            _currency = value
            notifyPropertyChanged(BR.currency)
        }

    var amount: Double
        @Bindable get() = _amount
        set(value) {
            _amount = value
            notifyPropertyChanged(BR.amount)
        }

    var picture: Int
        @Bindable get() = _picture
        set(value) {
            _picture = value
            notifyPropertyChanged(BR.picture)
        }

    var currencyName: Int
        @Bindable get() = _currencyName
        set(value) {
            _currencyName = value
            notifyPropertyChanged(BR.currencyName)
        }
}