package com.revolut.challenge.model

/**
 * Created by pcamilo on 01/05/2020.
 */
class BaseData<T>(var success: Boolean, var data: T?)