package com.revolut.challenge.view.adapters

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.revolut.challenge.databinding.LayoutItemCurrenciesBinding
import com.revolut.challenge.model.CurrencyModel
import com.revolut.challenge.viewmodel.CurrencyViewModel
import java.util.*


/**
 * Created by pcamilo on 01/05/2020.
 */
class CurrencyAdapter(var currencies: MutableList<CurrencyModel>, val currencyViewModel: CurrencyViewModel) :
    RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = LayoutItemCurrenciesBinding.inflate(LayoutInflater.from(parent.context))
        return CurrencyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return currencies.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencies[position])
    }

    /**
     * Submit the list to adapter
     */
    fun submitList(data: ArrayList<CurrencyModel>) {
        this.currencies = data
        notifyItemRangeChanged(1, this.currencies.size, data)
    }

    inner class CurrencyViewHolder(private val binding: LayoutItemCurrenciesBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CurrencyModel) {
            binding.model = item
            binding.amount.setText(item.amount.toString())
            binding.tvCurrencyName.text = itemView.resources.getString(item.currencyName)

            binding.amount.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {

                    if (adapterPosition != 0) moveItemToTop(adapterPosition)

                    binding.amount.addTextChangedListener(object : TextWatcher {
                        override fun beforeTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun onTextChanged(
                            value: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                            if (adapterPosition == 0) {
                                val amount: Double = try {
                                    if (value == null || value.isEmpty() || value == ".")
                                        1.0
                                    else
                                        value.toString().toDouble()
                                } catch (e: NumberFormatException) {
                                    1.0
                                }

                                currencyViewModel.updateBaseCurrency(
                                    currencies.first().currency,
                                    amount
                                )
                            }
                        }

                        override fun afterTextChanged(p0: Editable?) {
                        }
                    })
                }
            }

            binding.rootView.setOnClickListener {
                if (adapterPosition != 0) moveItemToTop(adapterPosition)
            }

            binding.executePendingBindings()
        }

        /**
         * Move item clicked to top of the list and updates the Base and Amount
         */
        private fun moveItemToTop(fromPosition: Int) {

            Collections.swap(currencies, fromPosition, 0)
            notifyItemMoved(fromPosition, 0)
            binding.amount.requestFocus()

            currencyViewModel.updateBaseCurrency(
                currencies.first().currency,
                currencies.first().amount
            )
        }
    }
}