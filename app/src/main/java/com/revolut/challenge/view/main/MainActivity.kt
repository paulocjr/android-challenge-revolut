package com.revolut.challenge.view.main

import android.content.IntentFilter
import android.net.ConnectivityManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.revolut.challenge.R
import com.revolut.challenge.databinding.ActivityMainBinding
import com.revolut.challenge.utils.ConnectivityReceiver
import com.revolut.challenge.view.base.BaseActivity
import com.revolut.challenge.viewmodel.CurrencyViewModel


class MainActivity : BaseActivity<ActivityMainBinding, CurrencyViewModel>(R.layout.activity_main),
    ConnectivityReceiver.ConnectivityReceiverListener {

    private var mSnackBar: Snackbar? = null
    lateinit var connectivityReceiver: ConnectivityReceiver

    override fun initBinding() {
        getViewModel().showLoading()
        getLatestCurrencies()
    }

    override fun getViewModelClass(): Class<CurrencyViewModel> = CurrencyViewModel::class.java

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            getViewModel().isRequesting = false
            getViewModel().hideLoading()
            getViewModel().showErrorContent.set(true)
            snackConnection(
                getString(R.string.no_internet_connection))
        } else {
            getViewModel().isRequesting = true
            getViewModel().showLoading()
            getViewModel().showErrorContent.set(false)

            this.mSnackBar?.let {
                it.dismiss()
            }
            getLatestCurrencies()
        }
    }

    /**
     * Show message no internet connection
     */
    private fun snackConnection(message: String) {
        mSnackBar = Snackbar.make(findViewById(R.id.content_main), message, BaseTransientBottomBar.LENGTH_INDEFINITE)
        mSnackBar?.show()
    }

    override fun onResume() {
        super.onResume()
        initBroadcastReceiver()
    }

    /**
     * Initialize the BroadcastReceiver to lister internet connection
     */
    private fun initBroadcastReceiver() {
        this.connectivityReceiver = ConnectivityReceiver()
        this.connectivityReceiver.connectivityReceiverListener = this

        registerReceiver(
            this.connectivityReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onStop() {
        unregisterReceiver(this.connectivityReceiver)
        super.onStop()
    }

    /**
     * Call the service getting all latest currencies
     */
    private fun getLatestCurrencies() {
        getViewModel().getCurrencies()
    }
}
