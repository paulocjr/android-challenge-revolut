package com.revolut.challenge.view.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.revolut.challenge.BR
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by pcamilo on 01/05/2020.
 */
abstract class BaseActivity<T : ViewDataBinding, V : ViewModel> (@LayoutRes private val layoutResId: Int) : AppCompatActivity(),
    HasSupportFragmentInjector {

    private lateinit var mDataBinding: T
    private lateinit var mViewModel: V

    @Inject
    lateinit var dispatchingAndroidInjector : DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mDataBinding = DataBindingUtil.setContentView(this, layoutResId)

        getViewModelClass().let {
            mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(getViewModelClass())

            getBinding().lifecycleOwner = this
            getBinding().apply {
                setVariable(BR.viewModel, mViewModel)
            }
        }

        initBinding()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    /**
     * Returns the current binding of layout
     *
     * @return the T is a generic type
     */
    fun getBinding(): T = mDataBinding

    /**
     * Returns the current view model
     *
     * @return the T is a generic type
     */
    fun getViewModel(): V = mViewModel

    /**
     * Init the binding for layouts on Activity or Fragment
     */
    protected abstract fun initBinding()
    protected abstract fun getViewModelClass(): Class<V>
}