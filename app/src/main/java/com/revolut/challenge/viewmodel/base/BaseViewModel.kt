package com.revolut.challenge.viewmodel.base

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.revolut.challenge.model.BaseData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by pcamilo on 01/05/2020.
 */
open class BaseViewModel : ViewModel() {

    val showLoading = ObservableField<Int>(View.GONE)
    val showErrorContent = ObservableField<Boolean>(false)

    fun showLoading() = showLoading.set(View.VISIBLE)
    fun hideLoading() = showLoading.set(View.GONE)

    /**
     * Executes a repository request as an async task, observing its response in the main thread.
     *
     * @param Result
     * @param observable
     * @param showLoading
     * @param hideLoading
     * @return
     */
    protected fun <Result> getAsyncData(observable: Observable<BaseData<Result>>, showLoading: Boolean = false, hideLoading: Boolean = true): Observable<BaseData<Result>> {

        if (showLoading) showLoading()

        return observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                if (hideLoading) hideLoading()
            }
    }

}