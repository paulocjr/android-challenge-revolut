package com.revolut.challenge.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.revolut.challenge.model.CurrencyModel
import com.revolut.challenge.repository.CurrencyRepository
import com.revolut.challenge.repository.base.ObservableBaseData
import com.revolut.challenge.repository.base.onFailure
import com.revolut.challenge.repository.base.onSuccess
import com.revolut.challenge.service.models.Currency
import com.revolut.challenge.utils.transformToCurrencyList
import com.revolut.challenge.view.adapters.CurrencyAdapter
import com.revolut.challenge.viewmodel.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by pcamilo on 01/05/2020.
 */
class CurrencyViewModel @Inject constructor(private val repository: CurrencyRepository) :
    BaseViewModel() {

    var isRequesting: Boolean = false
    var base = MutableLiveData("EUR") // Default is EUR
    var amount = MutableLiveData(100.0)
    var currencyList = ArrayList<CurrencyModel>()
    private lateinit var adapter: CurrencyAdapter
    private lateinit var dispose: Disposable
    private var time = 1

    fun getCurrencies() {
        this.dispose = Observable.interval(time.toLong(), TimeUnit.SECONDS)
            .takeWhile {
                isRequesting
            }
            .subscribe {
                requestCurrencies()
            }

        Thread.sleep(1000)
    }

    private fun requestCurrencies() {
        getAsyncCurrencies().onSuccess {
            updateList(it)
        }.onFailure {
            Log.v("onFailure", "Error...")
            isRequesting = false
            showErrorContent.set(true)
        }.subscribe()
    }

    private fun getAsyncCurrencies(): ObservableBaseData<Currency> =
        getAsyncData(
            repository.getCurrencies(base.value.toString(), amount.value?.toDouble()!!),
            showLoading = false
        )

    /**
     * Set the adapter in XML
     */
    fun getCurrencyAdapter(): CurrencyAdapter {
        adapter = CurrencyAdapter(currencyList, this)
        return adapter
    }

    /**
     * Update list of currencies
     */
    private fun updateList(currency: Currency) {
        currencyList =
            currency.transformToCurrencyList(base.value.toString(), amount.value?.toDouble()!!)
        this.adapter.submitList(currencyList)
    }

    /**
     * Updates the base currency and value
     */
    fun updateBaseCurrency(currency: String, amount: Double) {
        this.base.value = currency
        this.amount.value = amount
    }
}