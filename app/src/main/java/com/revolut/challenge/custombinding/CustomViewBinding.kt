package com.revolut.challenge.custombinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.revolut.challenge.R
import com.squareup.picasso.Picasso

/**
 * Created by pcamilo on 01/05/2020.
 */
class CustomViewBinding {

    companion object {

        @JvmStatic
        @BindingAdapter("setAdapter")
        fun bindRecyclerViewAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            recyclerView.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("imageUrl")
        fun bindImageView(imageView: ImageView, resourceId: Int) {
            Picasso.get()
                .load(resourceId)
                .into(imageView)
        }
    }
}