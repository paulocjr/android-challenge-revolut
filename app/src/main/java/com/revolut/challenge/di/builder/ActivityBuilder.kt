package com.revolut.challenge.di.builder

import com.revolut.challenge.di.scope.Activity
import com.revolut.challenge.view.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by pcamilo on 01/05/2020.
 */
@Module
abstract class ActivityBuilder {

    @Activity
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity
}