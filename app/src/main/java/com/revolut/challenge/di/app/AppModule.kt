package com.revolut.challenge.di.app

import android.app.Application
import android.content.Context
import com.revolut.challenge.service.APIClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by pcamilo on 01/05/2020.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideApiClient() = APIClient()
}