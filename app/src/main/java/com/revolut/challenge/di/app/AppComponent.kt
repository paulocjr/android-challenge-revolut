package com.revolut.challenge.di.app

import android.app.Application
import com.revolut.challenge.RevolutChallengeApplication
import com.revolut.challenge.di.builder.ActivityBuilder
import com.revolut.challenge.di.builder.ViewModelBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by pcamilo on 01/05/2020.
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        ViewModelBuilder::class,
        ActivityBuilder::class,
        AndroidSupportInjectionModule::class]
)
interface AppComponent {

    fun inject(application: RevolutChallengeApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}