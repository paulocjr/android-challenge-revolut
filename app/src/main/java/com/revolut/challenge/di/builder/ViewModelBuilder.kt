package com.revolut.challenge.di.builder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.revolut.challenge.di.ViewModelKey
import com.revolut.challenge.viewmodel.CurrencyViewModel
import com.revolut.challenge.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by pcamilo on 01/05/2020.
 */
@Module
abstract class ViewModelBuilder {

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyViewModel::class)
    abstract fun bindCurrency(currencyViewModel: CurrencyViewModel): ViewModel

    // ViewModel Factory
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}