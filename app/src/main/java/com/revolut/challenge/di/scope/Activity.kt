package com.revolut.challenge.di.scope

import javax.inject.Scope

/**
 * Created by paulo on 07-09-2019.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Activity