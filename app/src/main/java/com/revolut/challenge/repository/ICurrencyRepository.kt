package com.revolut.challenge.repository

import com.revolut.challenge.repository.base.ObservableBaseData
import com.revolut.challenge.service.models.Currency


/**
 * Created by pcamilo on 01/05/2020.
 */
interface ICurrencyRepository {

    /**
     * Returns the latest currencies
     *
     * @param base the identifier of Base
     * @param amount the identifier of Amount
     * @return the currency list
     */
    fun getCurrencies(base: String, amount: Double) : ObservableBaseData<Currency>
}