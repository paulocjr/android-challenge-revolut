package com.revolut.challenge.repository

import androidx.lifecycle.MutableLiveData
import com.revolut.challenge.repository.base.ObservableBaseData
import com.revolut.challenge.repository.base.mapToBaseData
import com.revolut.challenge.repository.base.mapToBaseResponse
import com.revolut.challenge.service.APIClient
import com.revolut.challenge.service.CurrencyService
import com.revolut.challenge.service.models.BaseResponse
import com.revolut.challenge.service.models.Currency
import javax.inject.Inject


/**
 * Created by pcamilo on 01/05/2020.
 */
class CurrencyRepository @Inject constructor (apiClient: APIClient) : ICurrencyRepository {

    private val mCurrencyService: CurrencyService = apiClient.getRetrofit().create(CurrencyService::class.java)
    val data: MutableLiveData<BaseResponse<Currency>> = MutableLiveData()

    /**
     * Get latest currencies
     *
     * @param base the base to retrive currencies
     * @param amount the amount of currency
     * @return
     */
    override fun getCurrencies(base: String, amount: Double) : ObservableBaseData<Currency> {
        return mCurrencyService.getCurrencies(base, amount)
            .mapToBaseResponse()
            .mapToBaseData { it }
    }
}