package com.revolut.challenge

import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.revolut.challenge.view.main.MainActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MainInstrumentedTest {

    @JvmField
    @Rule
    val testRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun beforeTest() {

    }

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.revolut.challenge", appContext.packageName)
    }

    @Test
    fun testHasLoading() {
        onView(withId(R.id.loading)).check(matches(isDisplayed()))
    }

    @Test
    fun test4loadingContent () {
        onView(withId(R.id.loading)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.rvCurrencies)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun test4noError() {
        onView(withId(R.id.layoutContentError)).check(matches(withEffectiveVisibility(Visibility.GONE)))
    }
}
